#!/bin/bash
pathdir="/home/debatin/Desktop/ConstituicaoBR/"
rm -rf "/home/debatin/Desktop/ConstituicaoBR16KHz"
mkdir "/home/debatin/Desktop/ConstituicaoBR16KHz/"
cd "$pathdir"
for dir in *
do
    echo "$dir"
    pathsubdir="/home/debatin/Desktop/ConstituicaoBR/$dir"
    mkdir "/home/debatin/Desktop/ConstituicaoBR16KHz/$dir/"
    cd "$pathsubdir"
    for filewav in $(ls -1 *.wav)
    do
        ffmpeg -i $filewav -acodec pcm_s16le -ar 16000 "/home/debatin/Desktop/ConstituicaoBR16KHz/$dir/$subdir/$filewav"
        #echo "$filewav"
    done
    for filetxt in $(ls -1 *.txt)
    do
        cp "$pathsubdir/$filetxt" "/home/debatin/Desktop/ConstituicaoBR16KHz/$dir/$subdir/$filetxt"
        #echo "$filetxt"
    done
done
echo "Done!"
