package application.lapsbm;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class LapsBMKaldi {
    private static String diretorioPrincipal = "src"+ File.separator+"resources"+File.separator;
    private static String pastasPrincipal[] = {"train", "test"};
    private static HashSet<String> spk2gender = new HashSet();
    private static HashSet<String> wavscp = new HashSet();
    private static HashSet<String> text = new HashSet();
    private static HashSet<String> utt2spk = new HashSet();
    private static HashSet<String> corpus = new HashSet();

    public void CorpusTreatment() throws IOException {
        for (String pasta : pastasPrincipal) {
            spk2gender.clear();
            wavscp.clear();
            text.clear();
            utt2spk.clear();
            File pastasLocutores = new File(diretorioPrincipal + "LapsBM16KHz" + File.separator + pasta + File.separator);
            for (File pl : pastasLocutores.listFiles()) {
                String sexo = pl.getName().substring(7, 8).toLowerCase();
                spk2gender.add(pl.getName().replace("-", "") + " " + sexo);
                for (File arquivo : pl.listFiles()) {
                    if (arquivo.getName().endsWith(".wav")) {
                        String caminhoSplit[] = arquivo.getPath().split("/");
                        String nome = caminhoSplit[4].replace("-", "") + "_" + caminhoSplit[5].replace(".wav", "");
                        String caminho = "/home/debatin/Documents/Toolkits/kaldi-trunk/egs/ocsr/data/" + caminhoSplit[3] + File.separator + caminhoSplit[4].replace("-", "") + File.separator + caminhoSplit[5];
                        wavscp.add(nome + " " + caminho);
                        transferirArquivo(arquivo);
                    } else if(arquivo.getName().endsWith(".txt")) {
                        String caminhoSplit[] = arquivo.getPath().split("/");
                        String nome = caminhoSplit[4].replace("-", "") + "_" + caminhoSplit[5].replace(".txt", "");
                        BufferedReader lerArquivo = new BufferedReader(new FileReader(arquivo));
                        String texto = acordoOrtografico(lerArquivo.readLine()).trim();
                        text.add(nome + " " + texto);
                        utt2spk.add(nome + " " + caminhoSplit[4].replace("-", ""));
                        corpus.add(texto);
                    }
                }
            }
            gravarArquivos(pasta);
        }
        gravaArquivo("corpus.txt", corpus);
    }

    private void transferirArquivo(File arquivo) throws IOException {
        String caminhoSplit[] = arquivo.getPath().split("/");
        File diretorioDestino = new File(diretorioPrincipal + "lapsbm-kaldi" + File.separator + caminhoSplit[3] + File.separator + caminhoSplit[4].replace("-", "") + File.separator);
        diretorioDestino.mkdirs();
        File arquivoDestino = new File (diretorioDestino.getPath() + File.separator + caminhoSplit[5]);
        InputStream in = new FileInputStream(arquivo);
        OutputStream out = new FileOutputStream(arquivoDestino);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    private String acordoOrtografico(String linha) {
        linha = linha.replace("  ", " ");
        linha = linha.replace("idéia", "ideia");
        linha = linha.replace("cinqüenta", "cinquenta");
        linha = linha.replace("assembléia", "assembleia");
        linha = linha.replace("estréia", "estreia");
        linha = linha.replace("quorum", "quórum");
        linha = linha.replace("tranqüilidade", "tranquilidade");
        linha = linha.replace("prevêem", "preveem");
        linha = linha.replace("pólo", "polo");
        linha = linha.replace("agüentar", "aguentar");
        return linha;
    }

    private void gravarArquivos(String pasta) throws IOException {
        gravaArquivo(pasta+File.separator+"spk2gender", spk2gender);
        gravaArquivo(pasta+File.separator+"wav.scp", wavscp);
        gravaArquivo(pasta+File.separator+"text", text);
        gravaArquivo(pasta+File.separator+"utt2spk", utt2spk);
    }

    private void gravaArquivo(String arquivo, HashSet<String> hs) throws IOException {
        List<String> sortedList = new ArrayList(hs);
        if (arquivo != "corpus.txt") {
            Collections.sort(sortedList);
        }
        FileWriter gerarArquivo = new FileWriter(diretorioPrincipal + "lapsbm-kaldi" + File.separator + arquivo, true);
        int aux = 0;
        String conteudo = "";
        for (String linha : sortedList) {
            if (aux != 0) {
                conteudo += "\n";
            } else {
                aux = 1;
            }
            conteudo += linha;
        }
        conteudo += "\n";
        gerarArquivo.write(conteudo);
        gerarArquivo.close();
        System.out.println("Arquivo '" + arquivo + "' gravado");
    }
}
