package application.constituicaobr;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class ConstituicaoBRHTK {
    private static String diretorioPrincipal = "src"+ File.separator+"resources"+File.separator;
    private static String pastasPrincipal[] = {"train", "test"};
    private static HashSet<String> wordsmlf = new HashSet();
    private static HashSet<String> wavtrainlist = new HashSet();
    private static HashSet<String> wavtestlist = new HashSet();
    private static HashSet<String> testtranscription = new HashSet();
    private static HashSet<String> traintranscription = new HashSet();
    private static String locutor = "ConstituicaoM001";

    public void CorpusTreatment() throws IOException {
        for (String pasta : pastasPrincipal) {
            File arquivos = new File(diretorioPrincipal + "ConstituicaoBR16KHz" + File.separator + pasta + File.separator);
            for (File arquivo : arquivos.listFiles()) {
                String caminhoSplit[] = arquivo.getPath().split("/");
                if (arquivo.getName().endsWith(".wav")) {
                    transferirArquivo(arquivo);
                    String nome = "corpus/" + locutor + "/" + caminhoSplit[4];
                    if (pasta.equals("train")) {
                        wavtrainlist.add(nome);
                    } else {
                        wavtestlist.add(nome);
                    }
                } else if(arquivo.getName().endsWith(".txt")) {
                    transferirArquivo(arquivo);
                    String nome = "\"*/" + caminhoSplit[4].replace(".txt", ".lab") + '"';
                    BufferedReader lerArquivo = new BufferedReader(new FileReader(arquivo));
                    String texto = acordoOrtografico(lerArquivo.readLine()).trim();
                    wordsmlf.add(nome + "#" + texto);
                    if (pasta.equals("train")) {
                        traintranscription.add("<s> " + texto + " </s>");
                    } else {
                        testtranscription.add("<s> " + texto + " </s>");
                    }
                }
            }
        }
        gravarArquivos();
    }

    private void transferirArquivo(File arquivo) throws IOException {
        String caminhoSplit[] = arquivo.getPath().split("/");
        File diretorioDestino = new File(diretorioPrincipal + "constituicaobr-htk" + File.separator + "corpus" + File.separator + locutor + File.separator);
        diretorioDestino.mkdirs();
        File arquivoDestino;
        if(arquivo.getName().endsWith(".txt")) {
            arquivoDestino = new File(diretorioDestino.getPath() + File.separator + caminhoSplit[4].replace(".txt", ".lab"));
        } else {
            arquivoDestino = new File(diretorioDestino.getPath() + File.separator + caminhoSplit[4]);
        }
        InputStream in = new FileInputStream(arquivo);
        OutputStream out = new FileOutputStream(arquivoDestino);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    private String acordoOrtografico(String linha) {
        linha = linha.replace("   ", " ");
        linha = linha.replace("  ", " ");
        linha = linha.replace("ü", "u");
        linha = linha.replace("assembléia", "assembleia");
        linha = linha.replace("idéia", "ideia");
        return linha;
    }

    private void gravarArquivos() throws IOException {
        gravaArquivo("words.mlf", wordsmlf);
        gravaArquivo("wav_train.list", wavtrainlist);
        gravaArquivo("wav_test.list", wavtestlist);
        gravaArquivo("train.transcription", traintranscription);
        gravaArquivo("test.transcription", testtranscription);
    }

    private void gravaArquivo(String arquivo, HashSet<String> hs) throws IOException {
        List<String> sortedList = new ArrayList(hs);
        Collections.sort(sortedList);
        FileWriter gerarArquivo = new FileWriter(diretorioPrincipal + "constituicaobr-htk" + File.separator + arquivo, true);
        int aux = 0;
        String conteudo = "";
        if (arquivo.equals("words.mlf")) {
            conteudo = "#!MLF!#\n";
        }
        for (String linha : sortedList) {
            if (aux != 0) {
                conteudo += "\n";
            } else {
                aux = 1;
            }
            if (arquivo.equals("words.mlf")) {
                String nomeFrase[] = linha.split("#");
                conteudo += nomeFrase[0] + "\n";
                String frase[] = nomeFrase[1].split(" ");
                for (int i = 0; i < frase.length; i++) {
                    conteudo += frase[i] + "\n";
                }
                conteudo += ".";
            } else {
                conteudo += linha;
            }
        }
        conteudo += "\n";
        gerarArquivo.write(conteudo);
        gerarArquivo.close();
        System.out.println("Arquivo '" + arquivo + "' gravado");
    }
}
